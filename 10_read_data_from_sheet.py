from __future__ import annotations

import contextlib
import functools
import os
import pathlib
import pickle
import sqlite3
from dataclasses import dataclass

from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.discovery import build
from google.oauth2.credentials import Credentials


@dataclass
class SheetConfig:
    sheet_id: str                        # 1EABCw834E80XXXXXXXXXXXXXXXXXXXXXXXXXXXXXq_c
    sheet_name_programs: str             # 'Треки мастерских'    # noqa: RUF003
    sheet_name_contacts: str             # 'Контакты мастерких'  # noqa: RUF003
    local_range_programs_no_header: str  # A1:Z'
    local_range_contacts_no_header: str  # A1:Z'
    programs_header: list[str]           # ['email_group', 'workshop_slug', ...]
    contacts_header: list[str]           # ['workshop', 'email_group', ...]

    @property
    def data_range_programs(self):
        return f'{self.sheet_name_programs}!{self.local_range_programs_no_header}'

    @property
    def data_range_contacts(self):
        return f'{self.sheet_name_contacts}!{self.local_range_contacts_no_header}'

    @property
    def ddl_statement_programs(self):
        fields = ', '.join(f'{column} TEXT' for column in self.programs_header)
        return f'CREATE TABLE programs ({fields})'

    @property
    def ddl_statement_contacts(self):
        fields = ', '.join(f'{column} TEXT' for column in self.contacts_header)
        return f'CREATE TABLE contacts ({fields})'

    @classmethod
    def from_env(cls) -> SheetConfig:
        programs_header = os.getenv('SHEET_COLUMNS_PROGRAMS').lower().split(',')
        contacts_header = os.getenv('SHEET_COLUMNS_CONTACTS').lower().split(',')
        range_programs = os.getenv('SHEET_LOCAL_RANGE_PROGRAMS_NO_HEADER')
        range_contacts = os.getenv('SHEET_LOCAL_RANGE_CONTACTS_NO_HEADER')

        return cls(
            sheet_id=os.getenv('SHEET_ID'),
            sheet_name_programs=os.getenv('SHEET_NAME_PROGRAMS'),
            sheet_name_contacts=os.getenv('SHEET_NAME_CONTACTS'),
            local_range_programs_no_header=range_programs,
            local_range_contacts_no_header=range_contacts,
            programs_header=programs_header,
            contacts_header=contacts_header,
        )

    @functools.cached_property
    def google_sheets_credentials(self):
        creds = None
        token_path = pathlib.Path(os.getenv('CLIENT_SECRET_TOKEN_PICKLE_CREDS_FILE'))

        if token_path.exists():
            with token_path.open('rb') as token:
                creds = pickle.load(token)

        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                creds_json_path = os.getenv('CLIENT_SECRET_JSON_CREDS_FILE')
                flow = InstalledAppFlow.from_client_secrets_file(
                    creds_json_path,
                    ['https://www.googleapis.com/auth/spreadsheets'],
                )
                creds = flow.run_local_server(port=0)

            with token_path.open('wb') as token:
                pickle.dump(creds, token)

        return creds


@dataclass
class SheetReader:
    creds: Credentials

    @functools.cached_property
    def api(self):
        return build('sheets', 'v4', credentials=self.creds)

    def query(self, sheet_id: str, sheet_range: str) -> list[str]:
        query = (
            self.api
            .spreadsheets()
            .values()
            .get(spreadsheetId=sheet_id, range=sheet_range)
            .execute()
        )
        return query.get('values', [])


def main(config: SheetConfig, db_url: str):
    print('Fetching programs and contacts data')
    reader = SheetReader(creds=config.google_sheets_credentials)
    query = functools.partial(reader.query, sheet_id=config.sheet_id)
    programs_data = query(sheet_range=config.data_range_programs)
    contacts_data = query(sheet_range=config.data_range_contacts)


    print(f'Saving data to sqlite3 {db_url=}')
    with contextlib.closing(sqlite3.connect(db_url)) as conn:
        cursor = conn.cursor()

        cursor.execute(config.ddl_statement_programs)
        values = ', '.join('?' * len(config.programs_header))
        for row in programs_data:
            cursor.execute(f'INSERT INTO programs VALUES ({values})', row)  # noqa

        cursor.execute(config.ddl_statement_contacts)
        values = ', '.join('?' * len(config.contacts_header))
        for row in contacts_data:
            cursor.execute(f'INSERT INTO contacts VALUES ({values})', row)  # noqa

        conn.commit()

    print('👍 Done.')



if __name__ == '__main__':
    db_url = os.getenv('SQLITE3_DB_URL')
    if pathlib.Path(db_url).exists():
        raise OSError(f'not rewriting db at {db_url=}')  # noqa
    main(SheetConfig.from_env(), db_url=db_url)
