from __future__ import annotations

import contextlib
import os
import pathlib
import sqlite3
from dataclasses import dataclass, asdict

import yaml


@dataclass
class DB_Config:
    db_url: str                             # os.getenv('SQLITE3_DB_URL')
    programs_columns: list[str]             # ['email_group', 'workshop_slug', ...]
    contacts_columns: list[str]             # ['workshop', 'email_group', ...]
    join_column: str                        # 'email_group'
    main_coordinator_email_column: str      # 'main_contact_email'

    @classmethod
    def from_env(cls) -> DB_Config:
        programs_header = os.getenv('SHEET_COLUMNS_PROGRAMS').lower().split(',')
        contacts_header = os.getenv('SHEET_COLUMNS_CONTACTS').lower().split(',')
        join_column = os.getenv('SQLITE3_JOIN_COLUMN')
        account_email_column = os.getenv('SQLITE3_MAIN_COORDINATOR_EMAIL_COLUMN')
        db_url = os.getenv('SQLITE3_DB_URL')

        return cls(
            db_url=db_url,
            programs_columns=programs_header,
            contacts_columns=contacts_header,
            join_column=join_column,
            main_coordinator_email_column=account_email_column,
        )

    @property
    def workshops_query(self):
        return """
            SELECT
                p.workshop_slug as slug,
                p.workshop_name as name,
                p.email_group as email
            FROM programs p
            GROUP BY 1,2,3
        """

    def generate_programs_query(self, workshop_slug):
        return f"""
            SELECT
                p.program_slug as slug,
                p.program_name as name
            FROM programs p
            WHERE workshop_slug='{workshop_slug}'
        """

    def generate_coord_emails_query(self, workshop_slug):
        return f"""
            SELECT
                DISTINCT(c.{self.main_coordinator_email_column}) as email
            FROM programs p
            JOIN
              contacts c
              ON p.{self.join_column} = c.{self.join_column}
            WHERE p.workshop_slug='{workshop_slug}'
        """

    @contextlib.contextmanager
    def cursor(self, commit=False):  # noqa
        with contextlib.closing(sqlite3.connect(self.db_url)) as conn:
            conn.row_factory = sqlite3.Row
            cursor = conn.cursor()
            yield cursor
            if commit:
                cursor.commit()


# DATA MODEL

@dataclass
class Account:
    email: str
    reset_password: bool

    @classmethod
    def from_row(cls, row: dict[str, str]) -> Account:
        return cls(
            email=row['email'],
            reset_password=True,
        )



@dataclass
class Program:
    name: str
    slug: str

    @classmethod
    def from_row(cls, row: dict[str, str]) -> Program:
        return cls(
            name=row['name'],
            slug=row['slug'],
        )


@dataclass
class Workshop:
    slug: str
    name: str
    email: str
    accounts: list[Account]
    programs: list[Program]


@dataclass
class WorkshopConfig:
    description: str
    source_doc: str
    workshops: list[Workshop]


def main(config: DB_Config):
    ws = WorkshopConfig(
        description='Workshops for 2023 season of lsh',
        source_doc=os.getenv('SHEET_URL'),
        workshops=[],
    )

    print('Reading data from sqlite3')

    with config.cursor() as cursor:
        ws_rows = cursor.execute(config.workshops_query)
        for row in ws_rows.fetchall():
            ws_slug = row['slug']
            ws_name=row['name']
            ws_email=row['email']

            programs_rows = cursor.execute(config.generate_programs_query(workshop_slug=ws_slug))
            programs = list(map(Program.from_row, programs_rows.fetchall()))

            accounts_rows = cursor.execute(config.generate_coord_emails_query(workshop_slug=ws_slug))
            accounts = list(map(Account.from_row, accounts_rows.fetchall()))

            new_w = Workshop(
                slug=ws_slug,
                name=ws_name,
                email=ws_email,
                accounts=accounts,
                programs=programs,
            )
            ws.workshops.append(new_w)

    out = pathlib.Path(os.getenv('WORKSHOPS_YAML_OUT'))
    if out.exists():
        print('Heads up, yaml out file is about to be rewritten')
    with out.open('w') as fp:
        dump = yaml.dump(asdict(ws), allow_unicode=True)
        print('---', file=fp)
        print(dump, file=fp)
    print(f'=== Written result to {out}')


if __name__ == '__main__':
    main(DB_Config.from_env())
