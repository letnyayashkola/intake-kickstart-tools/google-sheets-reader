# Get that yaml file, Auto!Automatic!! edition

Сначала получить креды, дока прям в гугле: 
[python quickstart](https://developers.google.com/sheets/api/quickstart/python)

потом поправить envrc

```shell
cp envrc_template .envrc
direnv allow
```
ну и дальше уже жара конечно:

```shell
pip install --user requirements.txt
python 10_read_data_from_sheet.py
python 20_join_data_output_ws_yaml.py
```

получившийся `workshops_*.yaml` добавить потом _куда надо_

Obviously, there's a reference: https://www.youtube.com/watch?v=a9NMtvvLRuo

Кстати всё это наверное можно успеть сделать пока трек играет свои три минуты

oudatchi
